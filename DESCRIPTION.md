# The goal

Sadly, Reddit's flavor of Markdown won't syntax highlight your code if you try adding your language like this:

    ```js
    const foo = "bar";
    ```

This makes code snippets less readable. This userscript's goal is to add syntax highlighting to code, as well as add line numbers on the side. This should hopefully make reading code snippets on Reddit a bit more pleasant, instead of having to copy them over into your preferred text editor or IDE just to get the proper syntax highlighting.

- [Userscript website][website]
- [Userscript repository][repository]
- [Direct install link][userscript] (also available in the repo and website)

# How does it work?

The trickiest part about this userscript was handling the question: "How do I know what language this code snippet is written in?". Usually a file extension is a pretty good indicator of the programming language contained inside. The problem is that Reddit has no files to work it; it's just snippets of code.

This userscript uses [PrismJS](https://prismjs.com/) under the hood in order to do the syntax highlighting. At the time of writing this, PrismJS supports 152 different languages, so determining the language a code snippet is using is difficult.

I'll talk about some of the approaches I considered to tackle this problem, as well as the one I ended up picking.

## Naive Bayes classifier

At first I thought about using a naive Bayes classifier in order to tell the languages apart, but this approach had some problems:

1. The filesize of the userscript would be immense if I tried teaching the classifier with numerous source code files for all of the 152 supported languages.
2. Even if filesize wasn't an issue, I imagine that the classifier would guess wrogly often due to the sheer number of different languages as well as the similarities they might share.

## Letting the OP signify the language in their snippets

Like I said earlier, Reddit doesn't provide the functionality of specifying the language inside the code block. However, I considered allowing the person writing the post/comment to include the language in some fashion, and then having the userscript pick up on that.

This approach has the advantage of being the most reliable. However, the downfall is that I can't expect everyone to have this userscript installed. There would be a majority of people not signifying the language in their code snippets simply because they might not know about this userscript, or because they didn't format their code properly.

## Let the userscript user select the language themselves

This is the approach that I chose. Since I can't rely on people specifying the language of their code snippets, and since it's too time-consuming and difficult to programmatically determine the language of said snippet, I figured I'd let the user pick the language themselves.

As such, the userscript will attach a dropdown list containing all of the supported languages above every code block it detects. The user can then pick the language from the list to highlight the code.

The downfall is that the burden of picking the appropriate user is now put on the user. Recall that there are 152 supported languages which the user has to look through in order to find the one they need. This gets tedious fast.

### Introducing suggestion buttons

To mitigate this, I also added what I like to call "suggestion buttons" right next to the dropdown list. The suggestion buttons are supposed to be a quick way of selecting the language you're looking for. How does the userscript know which languages to suggest?

I might not have file extensions to work with on Reddit, but I do have subreddits. If you're browsing `r/javascript`, chances are that you'll want to syntax highlight code blocks in JavaScript often. Thus, the userscript will add JS as a suggestion button. The userscript recognizes subreddits which include programming language names and will add corresponding suggestion buttons.

However, this also gets tricky. What about subreddits whose names don't include a programming language at all? Or subreddits which often have more than one dominant language in use? For example, `r/webdev` and `r/frontend` fit both of those criterria. For subreddits such as this, it's impossible to programmatically determine which suggestion buttons to offer, so I opted to hardcode them in. It's be unrealistic for me to know what languages are often used on which subreddits, considering that there are so many supported languages. This means that some subreddits will have some suggestions missing which would be convenient to have. If you'd like to help me out with this part, you can do so by contributing to [this file][suggestions-file].

# Contributing

I also welcome any contributions to the project! Like I mentioned, the userscript relies on hard-coded values to determine which suggestion buttons to offer, and it's unrealistic for me to be able to know which suggestions to give for all of the 152 supported languages. If you frequent a subreddit for which the userscript doesn't offer proper suggestion buttons, I encourage you to contribute and add the suggestions yourself. All it takes is modifying a [single file][suggestions-file].

[repository]: https://gitlab.com/Asha20/reddit-code-highlight
[website]: https://asha20.gitlab.io/reddit-code-highlight
[userscript]: https://asha20.gitlab.io/reddit-code-highlight/reddit-code-highlight.user.js
[suggestions-file]: https://gitlab.com/Asha20/reddit-code-highlight/blob/master/src/suggestions.ts