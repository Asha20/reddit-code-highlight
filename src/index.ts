import Dropdown from "./Dropdown";
import languages, {Language} from "./languages";
import getSuggestions, {getCurrentSubreddits} from "./getSuggestions";
import * as Prism from "prismjs";
import "./vendor/prism.css";

interface PrismEnvironment {
  code: string;
  element: HTMLElement;
  grammar?: string;
  language: string;
  plugins: Record<string, boolean>;
}

function highlightAll(parent: ParentNode = document) {
  (parent.querySelectorAll("pre code") as NodeListOf<HTMLElement>)
    .forEach(code => {
      if (code.hasAttribute("data-rch-highlighted")) {
        return;
      }

      code.classList.add("line-numbers", "language-none");
      code.setAttribute("data-rch-highlighted", "true");
      Prism.highlightElement(code);

      // HACK:
      // Reddit's CSS sets the parent <pre> element's display to grid.
      // This interferes with the Line Numbers plugin and makes the numbering
      // completely invisible. Removing the grid display makes the numbering
      // appear, but it isn't aligned properly to the lines of code. For some
      // reason, setting the display to "table" on the <pre> element itself
      // and making sure that the line-height is set to 1 fixes the invisible line
      // numbers and aligns them to the code properly.
      const pre = code.parentElement!;
      pre.style.display = "table";
      pre.style.lineHeight = "1";
      // Ensure that the toolbar sits inside the code block.
      pre.style.width = "100%";
    });
}

function rehighlightCode(code: HTMLElement, lang: string) {
  const content = code.textContent;
  const pre = code.parentElement!;
  const toolbar = pre.nextElementSibling as HTMLElement;

  const activeSuggestion = toolbar.querySelector(".rch-active");
  if (activeSuggestion) {
    activeSuggestion.classList.remove("rch-active");
  }

  const suggestionButton = toolbar.querySelector(`[data-lang="${lang}"]`);
  if (suggestionButton) {
    suggestionButton.classList.add("rch-active");
  }

  const currentLanguage = code.className.match(/language-[\w-]+/g);
  if (currentLanguage && currentLanguage[0] === lang) {
    return;
  }
  code.className = code.className.replace(/language-[\w-]+/g, "");
  pre.className = pre.className.replace(/language-[\w-]+/g, "");
  code.classList.add(`language-${lang}`);
  code.innerHTML = "";
  code.textContent = content;
  Prism.highlightElement(code);
}

function registerSuggestionButton(language: Language) {
  Prism.plugins.toolbar.registerButton(`select-${language.prismName}`, (env: PrismEnvironment) => {
    const button = document.createElement("button");
    button.dataset.lang = language.prismName;
    button.classList.add("rch-suggestion-button");
    button.classList.toggle("rch-active", language.prismName === "none");
    button.textContent = language.display;

    // The toolbar plugin places its toolbar right after the corresponding
    // <pre> element.
    let toolbar: HTMLElement | undefined;
    button.addEventListener("click", e => {
      e.preventDefault();
      if (!toolbar) {
        toolbar = env.element.parentElement!.nextSibling as HTMLPreElement;
      }

      const activeSuggestion = toolbar.querySelector(".rch-active");
      if (activeSuggestion) {
        activeSuggestion.classList.remove("rch-active");
      }
      button.classList.add("rch-active");
      rehighlightCode(env.element, language.prismName);
    });
    return button;
  });
}

const subreddits = getCurrentSubreddits();
getSuggestions(subreddits).forEach(registerSuggestionButton);
Prism.plugins.toolbar.registerButton("dropdown", (env: PrismEnvironment) => {
  const dropdown = new Dropdown(
    languages.map(({display, prismName}) => ({display, value: prismName})),
    language => rehighlightCode(env.element, language),
    (obj1, obj2) => {
      if (obj1.value === "none") {
        return -1;
      }
      if (obj2.value === "none") {
        return 1;
      }
      return obj1.display.localeCompare(obj2.display);
    },
  );
  return dropdown.element;
});

const observer = new MutationObserver(mutations => {
  for (const mutation of mutations) {
    if (mutation.addedNodes.length && mutation.addedNodes[0].nodeName === "DIV") {
      highlightAll(mutation.target as unknown as ParentNode);
    }
  }
});

// HACK:
// For some reason, when trying to highlight as soon as the document loads, the code
// blocks will highlight, but then immediately revert back to their original state.
// Adding a delay seems to fix the issue.
setTimeout(highlightAll, 500);
observer.observe(document.body, {childList: true, subtree: true});
