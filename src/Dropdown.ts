interface Option<T> {
  display: string;
  value: T;
}

type Sorter<T> = (obj1: T, obj2: T) => number;
type Handler<T> = (value: T) => void;

const noSort: Sorter<any> = () => 0;

export default class Dropdown<T> {
  element: HTMLSelectElement;
  private values: T[];
  private onChange: Handler<T>;

  constructor(options: Array<Option<T>>, onChange: Handler<T>, sortFn: Sorter<Option<T>> = noSort) {
    const sortedOptions = options.slice().sort(sortFn);
    this.element = document.createElement("select");
    this.values = sortedOptions.map(option => option.value);
    this.onChange = onChange;

    sortedOptions
      .map(({display}, i) => {
        const option = document.createElement("option");
        option.value = String(i);
        option.textContent = display;
        return option;
      })
      .forEach(option => this.element.appendChild(option));

    this.element.addEventListener("change", () => {
      onChange(this.selected());
    });
  }

  selected() {
    const selectedOption = this.element.options[this.element.selectedIndex];
    const selectedIndex = Number(selectedOption.value);
    return this.values[selectedIndex];
  }

  select(predicate: (value: T) => boolean) {
    const index = this.values.findIndex(predicate);
    if (index > -1) {
      this.element.selectedIndex = index;
    }
  }
}
