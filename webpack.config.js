const webpack = require("webpack");
const path = require("path");

const METADATA = `
// ==UserScript==
// @name Reddit Code Highlight
// @namespace https://gitlab.com/Asha20
// @match *://*.reddit.com/*
// @grant none
// ==/UserScript==
`;

module.exports = {
  context: __dirname,
  mode: "development",
  devtool: "inline-source-map",
  entry: "./src/index.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/dist",
    filename: "reddit-code-highlight.user.js",
  },

  resolve: {
    extensions: [".js", ".json", ".ts"],
    alias: {
      prismjs$: path.resolve(__dirname, "src", "vendor", "prism"),
    },
  },

  module: {
    rules: [
      {test: /\.ts$/, loader: "ts-loader"},
      {test: /\.css$/, use: ["style-loader", "css-loader"]},
    ],
  },

  plugins: [
    new webpack.BannerPlugin({
      banner: METADATA,
      raw: true,
      entryOnly: true,
    }),
  ]
};
