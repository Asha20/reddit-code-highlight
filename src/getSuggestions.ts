import suggestions from "./suggestions";
import languages, {PrismLanguage} from "./languages";

/*
 * Subreddits for programmming languages often have their "learning"
 * twins, such as "javascript" and "learnjavascript". Or in some cases,
 * like with the D programming language, its subreddit is actually called
 * "d_language", not just "d". This function strips the common prefixes
 * and suffixes and leaves only the name of the language.
 */
function removeSubredditVariations(subreddit: string) {
  const variations = [
    /^learn_?/,
    /_language$/,
    /lang$/,
  ];

  return variations.reduce((acc, regex) => acc.replace(regex, ""), subreddit);
}

export function getCurrentSubreddits() {
  const subreddit = location.href.match(/r\/[\w+]+/);
  return subreddit
    ? subreddit[0].slice(2).toLowerCase().split("+")
    : [];
}

/*
 * How the userscript decides which languages to suggest:
 *
 * 1. Always add "Plain Text".
 * 2. Look at the name of the subreddit. If the name is a valid PrismJS language,
 *    such as "javascript", add it to the suggestions.
 * 3. If the subreddit is inside a suggestion group, add all of the languages
 *    from that suggestion group.
 * 4. Throw out any duplicates and sort the rest, but keep "Plain Text" first.
 */
export function getSuggestionsForSubreddit(subreddit: string) {
  const languagesToSuggest = new Set<PrismLanguage>(["none"]); // 1
  const cleanSubreddit = removeSubredditVariations(subreddit);
  const subredditIsALanguage = languages.some(lang => lang.prismName === cleanSubreddit); // 2
  if (subredditIsALanguage) {
    languagesToSuggest.add(cleanSubreddit as PrismLanguage);
  }

  for (const suggestion of suggestions) { // 3
    if (suggestion.languages.some(lang => lang === cleanSubreddit)) {
      suggestion.languages.forEach(lang => languagesToSuggest.add(lang));
    }
  }
  return languagesToSuggest;
}

/*
 * The getSuggestions method accepts an array of subreddits because it is possible
 * for the user to be visiting a multireddit, such as "javascript+css", in which case,
 * we want to include relevant suggestion buttons for both subreddits.
 */
export default function getSuggestions(subreddits: string[]) {
  const languageSet = subreddits
    .map(getSuggestionsForSubreddit)
    .reduce((acc, set) => new Set([...acc, ...set]));
  return Array.from(languageSet) // 4
    .sort((x1, x2) => {
      if (x1 === "none") {
        return -1;
      }
      if (x2 === "none") {
        return 1;
      }
      return x1.localeCompare(x2);
    })
    .map(name => {
      const lang = languages.find(l => l.prismName === name);
      if (!lang) {
        throw new Error(`Language "${name}" isn't a valid PrismJS language.`);
      }
      return lang;
    });
}
