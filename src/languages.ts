// tslint:disable-next-line max-line-length
export type PrismLanguage = "none" | "markup" | "css" | "clike" | "javascript" | "abap" | "actionscript" | "ada" | "apacheconf" | "apl" | "applescript" | "arduino" | "arff" | "asciidoc" | "asm6502" | "aspnet" | "autohotkey" | "autoit" | "bash" | "basic" | "batch" | "bison" | "brainfuck" | "bro" | "c" | "csharp" | "cpp" | "coffeescript" | "clojure" | "crystal" | "csp" | "css-extras" | "d" | "dart" | "diff" | "django" | "docker" | "eiffel" | "elixir" | "elm" | "erb" | "erlang" | "fsharp" | "flow" | "fortran" | "gedcom" | "gherkin" | "git" | "glsl" | "gml" | "go" | "graphql" | "groovy" | "haml" | "handlebars" | "haskell" | "haxe" | "http" | "hpkp" | "hsts" | "ichigojam" | "icon" | "inform7" | "ini" | "io" | "j" | "java" | "jolie" | "json" | "julia" | "keyman" | "kotlin" | "latex" | "less" | "liquid" | "lisp" | "livescript" | "lolcode" | "lua" | "makefile" | "markdown" | "markup-templating" | "matlab" | "mel" | "mizar" | "monkey" | "n4js" | "nasm" | "nginx" | "nim" | "nix" | "nsis" | "objectivec" | "ocaml" | "opencl" | "oz" | "parigp" | "parser" | "pascal" | "perl" | "php" | "php-extras" | "plsql" | "powershell" | "processing" | "prolog" | "properties" | "protobuf" | "pug" | "puppet" | "pure" | "python" | "q" | "qore" | "r" | "jsx" | "tsx" | "renpy" | "reason" | "rest" | "rip" | "roboconf" | "ruby" | "rust" | "sas" | "sass" | "scss" | "scala" | "scheme" | "smalltalk" | "smarty" | "sql" | "soy" | "stylus" | "swift" | "tap" | "tcl" | "textile" | "tt2" | "twig" | "typescript" | "vbnet" | "velocity" | "verilog" | "vhdl" | "vim" | "visual-basic" | "wasm" | "wiki" | "xeora" | "xojo" | "xquery" | "yaml";

export interface Language {
  display: string;
  prismName: PrismLanguage;
}

// Taken from https://prismjs.com/#languages-list
const languages: Language[] = [
  // The representation of the lack of a language.
  {display: "Plain Text", prismName: "none"},

  {display: "Markup", prismName: "markup"},
  {display: "CSS", prismName: "css"},
  {display: "C-like", prismName: "clike"},
  {display: "JavaScript", prismName: "javascript"},
  {display: "ABAP", prismName: "abap"},
  {display: "ActionScript", prismName: "actionscript"},
  {display: "Ada", prismName: "ada"},
  {display: "Apache Configuration", prismName: "apacheconf"},
  {display: "APL", prismName: "apl"},
  {display: "AppleScript", prismName: "applescript"},
  {display: "Arduino", prismName: "arduino"},
  {display: "ARFF", prismName: "arff"},
  {display: "AsciiDoc", prismName: "asciidoc"},
  {display: "6502 Assembly", prismName: "asm6502"},
  {display: "ASP.NET (C#)", prismName: "aspnet"},
  {display: "AutoHotkey", prismName: "autohotkey"},
  {display: "AutoIt", prismName: "autoit"},
  {display: "Bash", prismName: "bash"},
  {display: "BASIC", prismName: "basic"},
  {display: "Batch", prismName: "batch"},
  {display: "Bison", prismName: "bison"},
  {display: "Brainfuck", prismName: "brainfuck"},
  {display: "Bro", prismName: "bro"},
  {display: "C", prismName: "c"},
  {display: "C#", prismName: "csharp"},
  {display: "C++", prismName: "cpp"},
  {display: "CoffeeScript", prismName: "coffeescript"},
  {display: "Clojure", prismName: "clojure"},
  {display: "Crystal", prismName: "crystal"},
  {display: "Content-Security-Policy", prismName: "csp"},
  {display: "CSS Extras", prismName: "css-extras"},
  {display: "D", prismName: "d"},
  {display: "Dart", prismName: "dart"},
  {display: "Diff", prismName: "diff"},
  {display: "Django/Jinja2", prismName: "django"},
  {display: "Docker", prismName: "docker"},
  {display: "Eiffel", prismName: "eiffel"},
  {display: "Elixir", prismName: "elixir"},
  {display: "Elm", prismName: "elm"},
  {display: "ERB", prismName: "erb"},
  {display: "Erlang", prismName: "erlang"},
  {display: "F#", prismName: "fsharp"},
  {display: "Flow", prismName: "flow"},
  {display: "Fortran", prismName: "fortran"},
  {display: "GEDCOM", prismName: "gedcom"},
  {display: "Gherkin", prismName: "gherkin"},
  {display: "Git", prismName: "git"},
  {display: "GLSL", prismName: "glsl"},
  {display: "GameMaker Language", prismName: "gml"},
  {display: "Go", prismName: "go"},
  {display: "GraphQL", prismName: "graphql"},
  {display: "Groovy", prismName: "groovy"},
  {display: "Haml", prismName: "haml"},
  {display: "Handlebars", prismName: "handlebars"},
  {display: "Haskell", prismName: "haskell"},
  {display: "Haxe", prismName: "haxe"},
  {display: "HTTP", prismName: "http"},
  {display: "HTTP Public-Key-Pins", prismName: "hpkp"},
  {display: "HTTP Strict-Transport-Security", prismName: "hsts"},
  {display: "IchigoJam", prismName: "ichigojam"},
  {display: "Icon", prismName: "icon"},
  {display: "Inform 7", prismName: "inform7"},
  {display: "Ini", prismName: "ini"},
  {display: "Io", prismName: "io"},
  {display: "J", prismName: "j"},
  {display: "Java", prismName: "java"},
  {display: "Jolie", prismName: "jolie"},
  {display: "JSON", prismName: "json"},
  {display: "Julia", prismName: "julia"},
  {display: "Keyman", prismName: "keyman"},
  {display: "Kotlin", prismName: "kotlin"},
  {display: "LaTeX", prismName: "latex"},
  {display: "Less", prismName: "less"},
  {display: "Liquid", prismName: "liquid"},
  {display: "Lisp", prismName: "lisp"},
  {display: "LiveScript", prismName: "livescript"},
  {display: "LOLCODE", prismName: "lolcode"},
  {display: "Lua", prismName: "lua"},
  {display: "Makefile", prismName: "makefile"},
  {display: "Markdown", prismName: "markdown"},
  {display: "Markup templating", prismName: "markup-templating"},
  {display: "MATLAB", prismName: "matlab"},
  {display: "MEL", prismName: "mel"},
  {display: "Mizar", prismName: "mizar"},
  {display: "Monkey", prismName: "monkey"},
  {display: "N4JS", prismName: "n4js"},
  {display: "NASM", prismName: "nasm"},
  {display: "nginx", prismName: "nginx"},
  {display: "Nim", prismName: "nim"},
  {display: "Nix", prismName: "nix"},
  {display: "NSIS", prismName: "nsis"},
  {display: "Objective-C", prismName: "objectivec"},
  {display: "OCaml", prismName: "ocaml"},
  {display: "OpenCL", prismName: "opencl"},
  {display: "Oz", prismName: "oz"},
  {display: "PARI/GP", prismName: "parigp"},
  {display: "Parser", prismName: "parser"},
  {display: "Pascal", prismName: "pascal"},
  {display: "Perl", prismName: "perl"},
  {display: "PHP", prismName: "php"},
  {display: "PHP Extras", prismName: "php-extras"},
  {display: "PL/SQL", prismName: "plsql"},
  {display: "PowerShell", prismName: "powershell"},
  {display: "Processing", prismName: "processing"},
  {display: "Prolog", prismName: "prolog"},
  {display: ".properties", prismName: "properties"},
  {display: "Protocol Buffers", prismName: "protobuf"},
  {display: "Pug", prismName: "pug"},
  {display: "Puppet", prismName: "puppet"},
  {display: "Pure", prismName: "pure"},
  {display: "Python", prismName: "python"},
  {display: "Q (kdb+ database)", prismName: "q"},
  {display: "Qore", prismName: "qore"},
  {display: "R", prismName: "r"},
  {display: "React JSX", prismName: "jsx"},
  {display: "React TSX", prismName: "tsx"},
  {display: "Ren'py", prismName: "renpy"},
  {display: "Reason", prismName: "reason"},
  {display: "reST (reStructuredText)", prismName: "rest"},
  {display: "Rip", prismName: "rip"},
  {display: "Roboconf", prismName: "roboconf"},
  {display: "Ruby", prismName: "ruby"},
  {display: "Rust", prismName: "rust"},
  {display: "SAS", prismName: "sas"},
  {display: "Sass (Sass)", prismName: "sass"},
  {display: "Sass (Scss)", prismName: "scss"},
  {display: "Scala", prismName: "scala"},
  {display: "Scheme", prismName: "scheme"},
  {display: "Smalltalk", prismName: "smalltalk"},
  {display: "Smarty", prismName: "smarty"},
  {display: "SQL", prismName: "sql"},
  {display: "Soy (Closure Template)", prismName: "soy"},
  {display: "Stylus", prismName: "stylus"},
  {display: "Swift", prismName: "swift"},
  {display: "TAP", prismName: "tap"},
  {display: "Tcl", prismName: "tcl"},
  {display: "Textile", prismName: "textile"},
  {display: "Template Toolkit 2", prismName: "tt2"},
  {display: "Twig", prismName: "twig"},
  {display: "TypeScript", prismName: "typescript"},
  {display: "VB.Net", prismName: "vbnet"},
  {display: "Velocity", prismName: "velocity"},
  {display: "Verilog", prismName: "verilog"},
  {display: "VHDL", prismName: "vhdl"},
  {display: "vim", prismName: "vim"},
  {display: "Visual Basic", prismName: "visual-basic"},
  {display: "WebAssembly", prismName: "wasm"},
  {display: "Wiki markup", prismName: "wiki"},
  {display: "Xeora", prismName: "xeora"},
  {display: "Xojo (REALbasic)", prismName: "xojo"},
  {display: "XQuery", prismName: "xquery"},
  {display: "YAML", prismName: "yaml"},
];

export default languages;
