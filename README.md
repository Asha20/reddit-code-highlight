# Reddit Code Highlight

![Demo of the userscript](.gitlab/demo.gif)

## What does this userscript do?

- Adds line numbers to every code block on Reddit.

- Adds suggestion buttons at the top of every code block. Clicking on a button highlights the code block in the chosen language.

- Adds a dropdown containing all of the supported languages, in case the language you're looking for isn't a part of the suggestion buttons.

- Supports multireddits; it will add relevant suggestion buttons for every subreddit inside the multireddit.

## What does this userscript *not* do?

- Highlight the code block automatically using the correct syntax highlighting. It's tricky to figure out what language a given code snippet is written in, which is why the userscript opts to show you the relevant suggestion buttons and let you select the correct language yourself.

- Show relevant suggestion buttons for every single programming language. This userscript primarily uses the current subreddit name in order to decide which suggestion buttons to show; however, subreddits don't necessarily have a 1:1 mapping to programming languages used there. For example, on a web development subreddit you'd expect to find code snippets of HTML, CSS, JS, TS... All of these need to be hard-coded so that the userscript can offer relevant suggestions, but with so many programming languages out there, it's impossible for me to know which languages are often used together. If you frequent a programming related subreddit and this userscript doesn't provide proper suggestion buttons for it, I encourage you to contribute and add them in the `src/suggestions.ts` file.

## How do I install it?

Make sure you have an userscript manager installed such as Tampermonkey, Greasemonkey or Violentmonkey. This userscript is hosted on GitLab Pages, so all you have to do to install it is click [here](https://asha20.gitlab.io/reddit-code-highlight/reddit-code-highlight.user.js).

## Contributing

- Cloning the repository:
```
git clone https://gitlab.com/Asha20/reddit-code-highlight.git
```
- Installing dependencies:
```
npm install
```
- Starting the development server:
```
npm start
```
This will serve the userscript at `http://localhost:8080/dist/reddit-code-highlight.user.js`. Navigate to that URL and the script will then automatically get installed by your userscript manager. You can now start working on the userscript.
- Building the userscript:
```
npm run build
``` 