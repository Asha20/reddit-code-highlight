import {PrismLanguage} from "./languages";

export interface Suggestion {
  subreddits: string[];
  languages: PrismLanguage[];
}

const suggestions: Suggestion[] = [
  {
    subreddits: ["javascript", "webdev", "node", "typescript", "css"],
    languages: ["markup", "css", "javascript", "typescript"],
  },
];

export default suggestions;
